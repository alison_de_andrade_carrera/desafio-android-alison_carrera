package com.solutions.concrete.alison_carrera.githubjavapop.Model;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by alison-carrera on 29/10/17.
 */

public class Pull {

    private String html_url;
    private String state;
    private String title;
    private String body;
    private Date created_at;
    private User user;

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreated_at() {

        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = formatter.format(created_at);

        return date;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
