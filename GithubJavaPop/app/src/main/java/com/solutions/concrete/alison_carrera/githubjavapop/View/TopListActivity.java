package com.solutions.concrete.alison_carrera.githubjavapop.View;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class TopListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new TopListFragment();
    }

}
