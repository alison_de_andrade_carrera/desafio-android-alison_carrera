package com.solutions.concrete.alison_carrera.githubjavapop.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class Owner {

    @SerializedName("login")
    private String username;
    @SerializedName("avatar_url")
    private String url_img;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrl_img() {
        return url_img;
    }

    public void setUrl_img(String url_img) {
        this.url_img = url_img;
    }
}
