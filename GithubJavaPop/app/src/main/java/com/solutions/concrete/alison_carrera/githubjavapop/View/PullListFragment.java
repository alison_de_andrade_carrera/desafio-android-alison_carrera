package com.solutions.concrete.alison_carrera.githubjavapop.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.paginate.Paginate;
import com.solutions.concrete.alison_carrera.githubjavapop.Interfaces.GitService;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.Pull;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.PullItem;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.Repository;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.RetrofitGitServiceFactory;
import com.solutions.concrete.alison_carrera.githubjavapop.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class PullListFragment extends Fragment {

    private RecyclerView recyclerView;
    private PullAdapter pullAdapter;
    private GitService gitService;
    private int page_number = 1;
    private boolean isLoading = false;
    private boolean error = false;
    private String messageError;
    private boolean lock = false;
    private String repo_user;
    private String repo_name;
    private TextView opened;
    private TextView closed;
    private int open = 0;
    private int close = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String resp = (String) getActivity().getIntent().getSerializableExtra(PullListActivity.PULL_ID);

        String[] resp2 = resp.split("/");

        repo_user = resp2[0];
        repo_name = resp2[1];

        ((PullListActivity) getActivity()).setActionBarTitle(repo_name);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.pull_fragment, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.pull_recycler);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(linearLayoutManager);

        opened = (TextView)v.findViewById(R.id.PR_Opened);
        closed = (TextView)v.findViewById(R.id.PR_Closed);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());

        messageError = getString(R.string.fetch_error);

        recyclerView.addItemDecoration(dividerItemDecoration);

        if (savedInstanceState != null) {

            page_number = savedInstanceState.getInt("page");
            isLoading = savedInstanceState.getBoolean("isLoading");
            error = savedInstanceState.getBoolean("error");
            open = savedInstanceState.getInt("open");
            close = savedInstanceState.getInt("close");

            pullAdapter = new PullAdapter(savedInstanceState.getParcelableArrayList("list_data_pull"));
            recyclerView.setAdapter(pullAdapter);
            pullAdapter.notifyDataSetChanged();

            opened.setText(String.valueOf(open) + " opened");
            closed.setText("/   " + String.valueOf(close) + " closed (in context)");

        } else {
            pullAdapter = new PullAdapter(new ArrayList<>());
            recyclerView.setAdapter(pullAdapter);
        }

        gitService = new RetrofitGitServiceFactory().create();

        Paginate.Callbacks callbacks = new Paginate.Callbacks() {

            @Override
            public void onLoadMore() {

                if (!lock) {
                    lock = true;


                    gitService.getPullRepository(repo_user, repo_name)
                            .subscribeOn(Schedulers.io())
                            .toObservable()
                            .flatMap(r -> io.reactivex.Observable.fromIterable(r))
                            .map(r -> PullItem.create(r))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(result -> {
                                pullAdapter.add(result);
                                updateOpenClosedPull(result.getState());
                                isLoading = true;
                            }, throwable -> {
                                makeToast(messageError);
                                error = true;
                            }, () -> {
                                isLoading = false;
                                update_page();
                            });
                }

            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return error;
            }
        };

        Paginate.with(recyclerView, callbacks)
                .addLoadingListItem(true)
                .build();

        return v;
    }

    private void update_page() {
        this.page_number += 1;
        this.lock = false;
    }

    private void updateOpenClosedPull(String state)
    {
        if(state.equals("open"))
        {
            open++;
        }
        else if(state.equals("closed"))
        {
            close++;
        }

        opened.setText(String.valueOf(open) + " opened");
        closed.setText("/   " + String.valueOf(close) + " closed (in context)");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList("list_data_pull", new ArrayList<PullItem>(pullAdapter.getPulls()));
        outState.putBoolean("isloading", isLoading);
        outState.putBoolean("error", error);
        outState.putInt("page", page_number);
        outState.putInt("open", open);
        outState.putInt("close", close);
    }

    private void makeToast(String text) {
        if (getActivity() != null && isAdded())
            Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }


}
