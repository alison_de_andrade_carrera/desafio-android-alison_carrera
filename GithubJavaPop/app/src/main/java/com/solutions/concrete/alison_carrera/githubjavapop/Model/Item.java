package com.solutions.concrete.alison_carrera.githubjavapop.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class Item {

    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("full_name")
    private String full_name;
    @SerializedName("forks")
    private int num_fork;
    @SerializedName("stargazers_count")
    private int num_start;
    @SerializedName("owner")
    private Owner owner;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public int getNum_fork() {
        return num_fork;
    }

    public void setNum_fork(int num_fork) {
        this.num_fork = num_fork;
    }

    public int getNum_start() {
        return num_start;
    }

    public void setNum_start(int num_start) {
        this.num_start = num_start;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
