package com.solutions.concrete.alison_carrera.githubjavapop.View;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;

import com.solutions.concrete.alison_carrera.githubjavapop.R;

public abstract class SingleFragmentActivity extends AppCompatActivity {

    public static final String PULL_ID = "com.alison.carrera.pull";

    protected abstract Fragment createFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);

        FragmentManager fm = getSupportFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if(fragment == null)
        {
            fragment = createFragment();
            fm.beginTransaction().add(R.id.fragment_container, fragment).commit();
        }

    }

    public static Intent newPullIntent(Context packageContext, String full_name)
    {
        Intent intent = new Intent(packageContext, PullListActivity.class);
        intent.putExtra(PULL_ID,full_name);
        return intent;
    }
}
