package com.solutions.concrete.alison_carrera.githubjavapop.View;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.solutions.concrete.alison_carrera.githubjavapop.Model.Repository;

import java.util.List;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryHolder> {

    private List<Repository> repositories;

    public RepositoryAdapter(List<Repository> repositories)
    {
        this.repositories = repositories;
    }

    @Override
    public RepositoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        return new RepositoryHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(RepositoryHolder holder, int position) {

        Repository repository = repositories.get(position);
        holder.bind(repository);

    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public void add(Repository repo)
    {
        repositories.add(repo);
        notifyDataSetChanged();
    }

    public List<Repository> getRepo()
    {
        return this.repositories;
    }
}
