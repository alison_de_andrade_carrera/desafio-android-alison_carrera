package com.solutions.concrete.alison_carrera.githubjavapop.View;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.PullItem;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.Repository;
import com.solutions.concrete.alison_carrera.githubjavapop.R;

import org.w3c.dom.Text;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class PullHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Context context;

    private TextView title;
    private TextView body;
    private TextView date;
    private TextView username;
    private ImageView img;
    private TextView full_name;

    private PullItem item;

    public PullHolder(LayoutInflater inflater, ViewGroup parent)
    {
        super(inflater.inflate(R.layout.pull_list_item, parent, false));

        context = itemView.getContext();

        itemView.setOnClickListener(this);

        title = (TextView)itemView.findViewById(R.id.PRTitle);
        body = (TextView)itemView.findViewById(R.id.PRBody);
        full_name = (TextView)itemView.findViewById(R.id.full_name);
        username = (TextView)itemView.findViewById(R.id.PRUser);
        img = (ImageView)itemView.findViewById(R.id.PicAuthor);
        date = (TextView)itemView.findViewById(R.id.PR_Date);
    }

    public void bind(PullItem pull, int opened, int closed)
    {
        this.item = pull;
        title.setText(pull.getTitle());
        body.setText(pull.getBody());
        username.setText(pull.getLogin());
        date.setText(pull.getCreated_at());
        //Glide contains an intern image cache system.
        Glide.with(context).load(pull.getAvatar_url()).apply(RequestOptions.circleCropTransform()).into(img);
    }

    @Override
    public void onClick(View view) {

        //Abrir navegador
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getHtml_url()));
        context.startActivity(browserIntent);

    }
}
