package com.solutions.concrete.alison_carrera.githubjavapop.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class GitResponse {

    @SerializedName("total_count")
    private Integer totalCount;
    @SerializedName("incomplete_results")
    private Boolean incompleteResults;
    @SerializedName("items")
    private List<Item> items;

    public Integer getTotalCount() {
        return totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
