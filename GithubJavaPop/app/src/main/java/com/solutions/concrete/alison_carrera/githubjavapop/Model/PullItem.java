package com.solutions.concrete.alison_carrera.githubjavapop.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alison-carrera on 29/10/17.
 */

public class PullItem implements Parcelable {

    private String html_url;
    private String state;
    private String title;
    private String body;
    private String created_at;
    private String login;
    private String avatar_url;

    public PullItem() {}

    protected PullItem(Parcel in) {

        html_url = in.readString();
        state = in.readString();
        title = in.readString();
        body = in.readString();
        created_at = in.readString();
        login = in.readString();
        avatar_url = in.readString();

    }

    public static final Creator<PullItem> CREATOR = new Creator<PullItem>() {
        @Override
        public PullItem createFromParcel(Parcel in) {
            return new PullItem(in);
        }

        @Override
        public PullItem[] newArray(int size) {
            return new PullItem[size];
        }
    };

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public static PullItem create(Pull item)
    {
        PullItem pull = new PullItem();
        pull.setAvatar_url(item.getUser().getAvatar_url());
        pull.setBody(item.getBody());
        pull.setCreated_at(item.getCreated_at());
        pull.setHtml_url(item.getHtml_url());
        pull.setLogin(item.getUser().getLogin());
        pull.setTitle(item.getTitle());
        pull.setState(item.getState());

        return pull;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(html_url);
        parcel.writeString(state);
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(created_at);
        parcel.writeString(login);
        parcel.writeString(avatar_url);
    }
}
