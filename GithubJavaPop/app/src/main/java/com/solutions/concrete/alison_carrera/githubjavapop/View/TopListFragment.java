package com.solutions.concrete.alison_carrera.githubjavapop.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.paginate.Paginate;
import com.solutions.concrete.alison_carrera.githubjavapop.Interfaces.GitService;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.Repository;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.RetrofitGitServiceFactory;
import com.solutions.concrete.alison_carrera.githubjavapop.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class TopListFragment extends Fragment {

    private RecyclerView recyclerView;
    private RepositoryAdapter repAdapter;
    private GitService gitService;
    private int page_number = 1;
    private boolean isLoading = false;
    private boolean error = false;
    private String messageError;
    private boolean lock = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.top_fragment, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.top_recycler);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());

        messageError = getString(R.string.fetch_error);

        recyclerView.addItemDecoration(dividerItemDecoration);

        if (savedInstanceState != null) {

            page_number = savedInstanceState.getInt("page");
            isLoading = savedInstanceState.getBoolean("isLoading");
            error = savedInstanceState.getBoolean("error");

            repAdapter = new RepositoryAdapter(savedInstanceState.getParcelableArrayList("list_data_home"));
            recyclerView.setAdapter(repAdapter);
            repAdapter.notifyDataSetChanged();
        } else {
            repAdapter = new RepositoryAdapter(new ArrayList<>());
            recyclerView.setAdapter(repAdapter);
        }

        gitService = new RetrofitGitServiceFactory().create();

        Paginate.Callbacks callbacks = new Paginate.Callbacks() {

            @Override
            public void onLoadMore() {

                if (!lock) {
                    lock = true;
                    gitService.getRemotesRepository(page_number)
                            .subscribeOn(Schedulers.io())
                            .toObservable()
                            .map(r -> r.getItems())
                            .flatMap(r -> io.reactivex.Observable.fromIterable(r))
                            .map(r -> Repository.create(r))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(result -> {
                                repAdapter.add(result);
                                isLoading = true;
                            }, throwable -> {
                                makeToast(messageError);
                                error = true;
                            }, () -> {
                                isLoading = false;
                                update_page();
                            });
                }

            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return error;
            }
        };

        Paginate.with(recyclerView, callbacks)
                .addLoadingListItem(true)
                .build();


        return v;
    }

    private void update_page() {
        this.page_number += 1;
        this.lock = false;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList("list_data_home", new ArrayList<Repository>(repAdapter.getRepo()));
        outState.putBoolean("isloading", isLoading);
        outState.putBoolean("error", error);
        outState.putInt("page", page_number);

    }

    private void makeToast(String text) {
        if (getActivity() != null && isAdded())
            Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }

}
