package com.solutions.concrete.alison_carrera.githubjavapop.View;

import android.support.v4.app.Fragment;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class PullListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new PullListFragment();
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}
