package com.solutions.concrete.alison_carrera.githubjavapop.View;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.solutions.concrete.alison_carrera.githubjavapop.Model.PullItem;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.Repository;

import java.util.List;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class PullAdapter extends RecyclerView.Adapter<PullHolder> {

    private List<PullItem> pull_items;
    private int opened = 0;
    private int closed = 0;

    public PullAdapter(List<PullItem> repositories)
    {
        this.pull_items = repositories;
    }

    @Override
    public PullHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        return new PullHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(PullHolder holder, int position) {

        PullItem pullItem = pull_items.get(position);
        holder.bind(pullItem, opened, closed);

    }

    @Override
    public int getItemCount() {
        return pull_items.size();
    }

    public void add(PullItem items)
    {
        if(items.getState().equals("open"))
        {
            opened++;
        }
        else if(items.getState().equals("closed"))
        {
            closed++;
        }

        pull_items.add(items);
        notifyDataSetChanged();
    }

    public List<PullItem> getPulls()
    {
        return this.pull_items;
    }
}
