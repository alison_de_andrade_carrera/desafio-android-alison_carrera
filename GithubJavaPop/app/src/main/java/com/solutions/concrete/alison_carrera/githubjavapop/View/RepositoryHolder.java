package com.solutions.concrete.alison_carrera.githubjavapop.View;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.solutions.concrete.alison_carrera.githubjavapop.Model.Repository;
import com.solutions.concrete.alison_carrera.githubjavapop.R;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class RepositoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Context context;

    private TextView name;
    private TextView description;
    private TextView full_name;
    private TextView username;
    private ImageView img;
    private TextView num_fork;
    private TextView num_start;

    private Repository repo;

    public RepositoryHolder(LayoutInflater inflater, ViewGroup parent)
    {
        super(inflater.inflate(R.layout.top_list_item, parent, false));

        context = itemView.getContext();

        itemView.setOnClickListener(this);

        name = (TextView)itemView.findViewById(R.id.PRTitle);
        description = (TextView)itemView.findViewById(R.id.PRBody);
        full_name = (TextView)itemView.findViewById(R.id.full_name);
        username = (TextView)itemView.findViewById(R.id.PRUser);
        img = (ImageView)itemView.findViewById(R.id.PicAuthor);
        num_start = (TextView)itemView.findViewById(R.id.stars);
        num_fork = (TextView)itemView.findViewById(R.id.fork);
    }

    public void bind(Repository repo)
    {
        this.repo = repo;
        name.setText(repo.getName());
        description.setText(repo.getDescription());
        full_name.setText(repo.getFull_name());
        username.setText(repo.getUsername());
        //Glide contains an intern image cache system.
        Glide.with(context).load(repo.getUrl_img()).apply(RequestOptions.circleCropTransform()).into(img);
        num_fork.setText(String.valueOf(repo.getNum_fork()));
        num_start.setText(String.valueOf(repo.getNum_start()));
    }

    @Override
    public void onClick(View view) {

        Intent intent = PullListActivity.newPullIntent(context, repo.getFull_name());
        context.startActivity(intent);

    }
}
