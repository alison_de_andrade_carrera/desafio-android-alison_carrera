package com.solutions.concrete.alison_carrera.githubjavapop.Model;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.solutions.concrete.alison_carrera.githubjavapop.Interfaces.GitService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alison-carrera on 28/10/17.
 */

public class RetrofitGitServiceFactory {

    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

    Retrofit retrofit = new Retrofit.Builder().client(client).addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.github.com/").build();

    public GitService create()
    {
        return retrofit.create(GitService.class);
    }

}
